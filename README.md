# Image Uploader Bot

Bot resizes given images to 640x640 in jpg format and saves them to remote cloud storage.
The workflow should is divided into the following independent steps:
1. Schedule list of images to be processed.
2. Resize scheduled images.
3. Upload resized image to cloud storage.

If any step fails files are moved to failed queue.

## Installation

1. Ensure your JAVA_HOME is properly set.
2. You're all done.

## Usage

You can get this output running "bot" without parameters.

command [arguments]

Available commands:

1. *schedule* <path> Add filenames to resize queue

2. *resize* [-n count] Resize next images from the queue

3. *status* Output current status in format %queue%:%number_of_images%

4. *upload* [-n count] Upload next count of images to remote storage

5. *retry* [-n count] Moves next count of images from failed queue back to resize queue.

## Additional info

At first attempt to upload bot would ask you to authorize him.
Later on it stores dropbox access key in config.json (created after authorization process).
Should you need to update token just delete this file and on next upload bot will ask for authorization.
If for some reason bot would be unable to store auth token it would ask for it every upload run.

Bot uses embedded ActiveMQ broker. Persistent storage is located under "activemq-data" directory near bot jar (created at 1st run).
If you need to reset bot state just delete "activemq-data" directory.
