/*
 * The MIT License
 *
 * Copyright 2016 jet800.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.jet800.imagebot;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuth;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.UploadUploader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.jms.JMSException;
import org.apache.commons.io.FilenameUtils;

/**
 * Worker that uploads resized images from "upload" queue to Dropbox storage.
 *
 * @author jet800
 */
public class UploadWorker extends QueueWorker {

    private static final String APP_KEY = "2v2oi1unuiktw1z";
    private static final String SECRET_KEY = "m6q5uwb2k7g879e";

    private static final String CONFIG_LOCATION = "config.json";

    private final DbxClientV2 client;

    private final Configuration configuration;

    public UploadWorker(int n) throws Exception {
        super("upload", "done", n);
        this.configuration = Configuration.fromJson(CONFIG_LOCATION);
        DbxRequestConfig requestConfig = new DbxRequestConfig("text-edit/0.1");
        String token = getToken(requestConfig);
        if (token != null) {
            client = new DbxClientV2(requestConfig, token);
        } else {
            client = null;
        }
    }

    @Override
    public void run() throws JMSException {
        if (client != null) { // skip iterating over files if DropBox client was not initialized
            super.run();
        }
    }

    @Override
    protected File process(File file) throws Exception {
        UploadUploader uploader = client.files().upload("/" + FilenameUtils.getName(file.getAbsolutePath()));
        try (FileInputStream fis = new FileInputStream(file)) {
            uploader.uploadAndFinish(fis);
        }

        return file;
    }

    private String getToken(DbxRequestConfig requestConfig) throws DbxException, IOException {
        if (configuration.getToken() != null && !configuration.getToken().isEmpty()) {
            return configuration.getToken();
        }
        DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, SECRET_KEY);
        DbxWebAuth auth = new DbxWebAuth(requestConfig, appInfo);

        DbxWebAuth.Request authRequest = DbxWebAuth.newRequestBuilder()
                .withNoRedirect()
                .build();
        String authorizeUrl = auth.authorize(authRequest);
        System.out.println("1. Go to " + authorizeUrl);
        System.out.println("2. Click \"Allow\" (you might have to log in first).");
        System.out.println("3. Copy the authorization code.");
        System.out.print("Enter the authorization code here(or press enter to cancel): ");

        String code = System.console().readLine();
        if (code != null) {
            code = code.trim();
            DbxAuthFinish authFinish = auth.finishFromCode(code);
            String token = authFinish.getAccessToken();
            configuration.setToken(token);
            configuration.toJson(CONFIG_LOCATION);
            return token;
        } else {
            return null;
        }
    }

}
