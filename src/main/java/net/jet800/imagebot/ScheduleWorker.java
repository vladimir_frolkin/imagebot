/*
 * The MIT License
 *
 * Copyright 2016 jet800.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.jet800.imagebot;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Worker that takes a directory and adds files to "resize" queue. It follows whole directory tree and adds all files
 * with png, jpg or jpeg extensions.
 *
 * @author jet800
 */
public class ScheduleWorker extends AbstractWorker {

    private static final Logger LOG = Logger.getLogger(ScheduleWorker.class.getName());
    private static final PathMatcher MATCHER = FileSystems.getDefault().getPathMatcher("regex:(?iu).*\\.(?:png|jpe?g)$");

    private final Path source;

    public ScheduleWorker(Path source) throws Exception {
        this.source = source;
        successSender = session.createSender(session.createQueue("resize"));
    }

    @Override
    protected File process(File file) {
        try {
            successSender.send(session.createTextMessage(file.getAbsolutePath()));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Unable to schedule [" + file + "]", ex);
        }
        return file;
    }

    @Override
    public void run() throws IOException {
        Files.walkFileTree(source, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (MATCHER.matches(file)) {
                    System.out.println("Scheduling: " + file);
                    process(file.toFile());
                } else {
                    System.out.println("Skipping: " + file);
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

}
