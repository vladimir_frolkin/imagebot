/*
 * The MIT License
 *
 * Copyright 2016 jet800.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.jet800.imagebot;

import com.google.gson.Gson;
import java.io.FileReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author jet800
 */
public class Configuration {

    private String token;

    public static Configuration fromJson(String jsonLocation) {
        File file = new File(jsonLocation);

        try (FileReader configReader = new FileReader(file)) {
            Gson gson = new Gson();
            return gson.fromJson(configReader, Configuration.class);
        } catch (IOException ex) {
            return new Configuration(); // just work like there is no token stored
        }
    }

    public void toJson(String jsonLocation) throws IOException {
        File file = new File(jsonLocation);
        try (FileWriter configWriter = new FileWriter(file)) {
            Gson gson = new Gson();
            configWriter.append(gson.toJson(this));
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
