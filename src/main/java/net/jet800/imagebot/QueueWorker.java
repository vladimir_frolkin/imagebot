/*
 * The MIT License
 *
 * Copyright 2016 jet800.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.jet800.imagebot;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.QueueReceiver;
import javax.jms.TextMessage;

/**
 * Base class for Workers.
 *
 * @author jet800
 */
public abstract class QueueWorker extends AbstractWorker {

    private static final Logger LOG = Logger.getLogger(QueueWorker.class.getName());

    protected QueueReceiver receiver;
    private int n;

    public QueueWorker(String inbound, String success, int n) throws Exception {
        this.n = n;
        receiver = session.createReceiver(session.createQueue(inbound));
        successSender = session.createSender(session.createQueue(success));
    }

    @Override
    public void run() throws JMSException {
        while (n > 0) {
            TextMessage message = (TextMessage) receiver.receiveNoWait();
            if (message != null) {
                n--;
                System.out.println("Processing " + message.getText());
                try {
                    File file = new File(message.getText());
                    File result = process(file);
                    successSender.send(session.createTextMessage(result.getAbsolutePath()));
                    System.out.println("Processed to: " + result);
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, "Error processing [" + message.getText() + "]", ex);
                    retrySender.send(message);
                }
            } else {
                break;
            }
        }
    }

}
