/*
 * The MIT License
 *
 * Copyright 2016 jet800.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.jet800.imagebot;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;
import org.apache.commons.io.FilenameUtils;

/**
 * Worker that takes images from "resize" queue and resized them to 640x640 adding white background.
 * @author jet800
 */
public class ResizeWorker extends QueueWorker {

    private static final int IMAGE_SIZE = 640;

    public ResizeWorker(int n) throws Exception {
        super("resize", "upload", n);
    }

    @Override
    protected File process(File file) throws IOException {
        File resizedDir = new File("images_resized");
        if(!resizedDir.exists() &&
            !resizedDir.mkdir()) {
            throw new IllegalStateException("'images_resized' dir is not present and cannot be created!");
        }
        
        BufferedImage original = ImageIO.read(file);

        BufferedImage resized;

        BufferedImage tmp = Scalr.resize(original, IMAGE_SIZE);

        if (tmp.getHeight() == tmp.getWidth()) {
            resized = tmp;
        } else {
            int type = tmp.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : tmp.getType();
            resized = new BufferedImage(IMAGE_SIZE, IMAGE_SIZE, type);
            Graphics2D g = resized.createGraphics();
            g.setBackground(Color.WHITE);
            g.clearRect(0, 0, IMAGE_SIZE, IMAGE_SIZE);
            g.drawImage(tmp, 0, 0, tmp.getWidth(), tmp.getHeight(), null);
            g.dispose();
        }

        File result = new File("images_resized" + File.separator + FilenameUtils.getBaseName(file.getAbsolutePath()) + "." + Long.toHexString(System.currentTimeMillis()) + ".jpg");
        ImageIO.write(resized, "jpg", result);

        original.flush();
        resized.flush();
        
        Files.delete(file.toPath());
        return result;
    }

}
