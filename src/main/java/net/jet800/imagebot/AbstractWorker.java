/*
 * The MIT License
 *
 * Copyright 2016 jet800.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.jet800.imagebot;

import java.io.File;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;

/**
 * Base abstract class for processing files. Working with embedded ActiveMQ broker.
 *
 * @author jet800
 */
public abstract class AbstractWorker {

    protected QueueSender successSender, retrySender;

    protected final QueueSession session;
    protected final BrokerService broker;

    private final QueueConnection connection;

    public AbstractWorker() throws Exception {
        broker = new BrokerService();
        broker.setBrokerName("localhost");
        broker.setPersistent(true);
        broker.start();
        broker.waitUntilStarted();

        ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory("vm://localhost?create=false");
        connection = cf.createQueueConnection();
        connection.start();
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        Queue retryQueue = session.createQueue("failed");
        retrySender = session.createSender(retryQueue);

    }

    protected abstract File process(File file) throws Exception;

    public abstract void run() throws Exception;

    public void stop() throws Exception {
        session.close();
        connection.close();
        broker.stop();
        broker.waitUntilStopped();
    }

}
