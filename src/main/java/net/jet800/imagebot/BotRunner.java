/*
 * The MIT License
 *
 * Copyright 2016 jet800.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.jet800.imagebot;

import java.nio.file.Paths;

/**
 * Image Uploader Bot main class.
 *
 * @author jet800
 */
public class BotRunner {

    private static final String USAGE = "Uploader Bot\n"
            + "\n"
            + "Usage:\n"
            + "\n"
            + "command [arguments]\n"
            + "\n"
            + "Available commands:\n"
            + "\n"
            + "schedule <path> Add filenames to resize queue\n"
            + "\n"
            + "resize [-n count] Resize next images from the queue\n"
            + "\n"
            + "status Output current status in format %queue%:%number_of_images%\n"
            + "\n"
            + "upload [-n count] Upload next count of images to remote storage"
            + "\n"
            + "retry [-n count] Moves next count of images from failed queue back to resize queue.";

    private static final String SCHEDULE = "schedule";
    private static final String RESIZE = "resize";
    private static final String RETRY = "retry";
    private static final String UPLOAD = "upload";
    private static final String STATUS = "status";

    /**
     * @param args the command line arguments
     * @throws Exception if something unexpected happens
     */
    public static void main(String[] args) throws Exception {

        String command = args.length > 0 ? args[0] : "";

        AbstractWorker worker = null;
        try {
            switch (command) {
                case "":
                    System.out.println(USAGE);
                case SCHEDULE:
                    System.out.println("Schedule mode");
                    if (args.length > 0) {
                        worker = new ScheduleWorker(Paths.get(args[1]));
                    } else {
                        System.err.println("Nothing to shedule. Specify a directory to shedule");
                    }
                    break;
                case RESIZE:
                    System.out.println("Resize mode");
                    worker = new ResizeWorker(getCount(args));
                    break;
                case UPLOAD:
                    System.out.println("Upload mode");
                    worker = new UploadWorker(getCount(args));
                    break;
                case RETRY:
                    System.out.println("Retry mode");
                    worker = new RetryWorker(getCount(args));
                    break;
                case STATUS:
                    worker = new StatusWorker();
                    break;
                default:
                    System.err.println("Unknown command: " + args[1]);
                    System.out.println(USAGE);
            }
            if (worker != null) {
                worker.run();
            }
        } finally {
            if (worker != null) {
                worker.stop();
            }
        }
    }

    private static int getCount(String args[]) {
        int count = Integer.MAX_VALUE;
        if (args.length > 1 && "-n".equals(args[1])) {
            count = Integer.parseInt(args[2]);
        }
        return count;
    }

}
